class Potepan::ProductsController < ApplicationController
  DISPLAY_MAX_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).
      order("RAND()").limit(DISPLAY_MAX_PRODUCTS)
  end
end
