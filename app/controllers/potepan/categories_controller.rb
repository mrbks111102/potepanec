class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.all.includes([taxons: :products])
    @category = Spree::Taxon.find(params[:id])
    @products = @category.all_products.includes(master: [:default_price, :images])
  end
end
