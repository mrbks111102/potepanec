require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product) }
  let!(:related_products) { create(:product, taxons: [taxon]) }

  describe '#related_products' do
    it '関連商品にメインの商品は含まれないこと' do
      expect(product.related_products).not_to include product
    end

    it '商品データが取得されていること' do
      expect(product.related_products).to include related_products
    end
  end
end
