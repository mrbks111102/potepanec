require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:parent_taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: parent_taxon) }
  let!(:product) { create(:product, name: "同カテゴリー商品", taxons: [taxon]) }

  before do 
    get :show, params: { id: taxon.id }
  end

  it 'リクエストが成功する' do
    expect(response).to have_http_status(200)
  end

  it 'viewがレンダリングされている' do
    expect(response).to render_template :show
  end

  it "モデルオブジェクトが読み込まれている" do
    expect(assigns(:taxonomies).first).to eq taxonomy
    expect(assigns(:category)).to eq taxon
  end

  it "同カテゴリーの商品を表示する" do
    expect(assigns(:products)).to contain_exactly(product)
  end
end
