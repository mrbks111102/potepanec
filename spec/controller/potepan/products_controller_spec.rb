require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do 
    get :show, params: { id: product.id }
  end

  it 'リクエストが成功する' do
    expect(response.status).to eq(200)
  end

  it 'viewがレンダリングされている' do
    expect(response).to render_template :show
  end

  it "データが取得できている" do
    expect(assigns(:product)).to eq product
  end

  it "関連商品を4つ取得できている" do
    expect(assigns(:related_products).size).to eq 4
  end

  it "関連商品に現在商品詳細を開いている商品を含まない" do
    expect(assigns(:related_products)).not_to include product
  end
end
