require 'rails_helper'

RSpec.feature "Product", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let(:product2) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end
  
  it '商品詳細ページからHomeをクリックするとトップページにアクセスする' do
    expect(page).to have_link('Home', href: potepan_index_path)
  end

  it 'ロゴをクリックするとトップページにアクセスする' do
    within ".navbar-header" do
      link = find('.navbar-brand').click
      expect(link[:href]).to eq potepan_index_path
    end
  end
  
  it '名前、説明、価格が表示されていること' do
    within ".singleProduct" do
      expect(page).to have_content(product.name)
      expect(page).to have_content(product.description)
      expect(page).to have_content(product.display_price)
    end
  end

  it '一覧ページに戻るをクリックするとカテゴリー一覧へ戻る' do
    click_link '一覧ページへ戻る'
    expect(page).to have_content(taxon.name)
  end

  it '関連商品をクリックすると商品詳細ページに飛ぶ' do
    within ".productsContent" do
      click_on related_product.name
    end
    expect(current_path).to eq potepan_product_path(related_product.id)
  end

  it '関連しない商品は表示されない' do
    within(".productsContent") do
      expect(page).not_to have_content product2.name
    end
  end
end
