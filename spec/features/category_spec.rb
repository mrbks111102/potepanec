require 'rails_helper'

RSpec.feature "Category", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, name: "Brand", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:child_taxon) { create(:taxon, name: "Bags", parent: taxon) }
  let!(:product) { create(:product, taxons: [child_taxon]) }
  
  before do
    visit potepan_category_path(taxon.id)
  end

  it "ページタイトルが動的に表示される" do
    within ".page-title" do
      expect(page).to have_content taxon.name
    end
  end

  it '商品詳細ページからHomeをクリックするとトップページにアクセスする' do
    expect(page).to have_link('Home', href: potepan_index_path)
  end

  it 'カテゴリーが表示される' do
    expect(page).to have_content taxon.name
    expect(page).to have_content child_taxon.name
  end

  it 'カテゴリーに属した商品の名前、価格が表示される' do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  it '商品をクリックすると商品詳細ページが表示される' do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
