require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "Application Title helpers" do
    context '各ページに対して' do
      it "完全なタイトルを返す" do
        expect(full_title("test")).to eq "test - BIGBAG Store"
      end
    end

    context '引数がnilの場合' do
      it "ベースタイトルを返す" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end

    context '引数が "" の場合' do
      it "ベースタイトルを返す" do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end
  end
end
